use std::error::Error;
use std::fs;

fn main() -> Result<(), Box<dyn Error>>
{
	let input = fs::read_to_string("../day2.txt").unwrap();

	let mut count1: u16 = 0;
	let mut count2: u16 = 0;
	for line in input.lines() {
		let groups: Vec<&str> = line.split_whitespace().collect();
		let dash = groups[0].find('-').unwrap();

		let start = groups[0].get(0..dash).unwrap().parse::<u8>()?;
		let end = groups[0].get(dash + 1..).unwrap().parse::<u8>()?;
		let key = groups[1].chars().next().unwrap();
		let times = groups[2].matches(key).count() as u8;

		if (start..=end).contains(&times) {
			count1 += 1;
		}

		let first = groups[2].chars().nth((start - 1) as usize).unwrap();
		let last = groups[2].chars().nth((end - 1) as usize).unwrap();
		if (first == key) ^ (last == key) {
			count2 += 1;
		}
	}
	println!("Part 1: {}", count1);
	println!("Part 2: {}", count2);
	Ok(())
}
