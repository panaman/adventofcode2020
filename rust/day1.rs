use std::error::Error;
use std::fs;

fn main() -> Result<(), Box<dyn Error>>
{
	let file = fs::read_to_string("../day1.txt")?;

	let mut input: Vec<u16> = Vec::with_capacity(200);
	for line in file.lines() {
		input.push(line.parse::<u16>()?);
	}
	input.sort_unstable();

	part1(&input);
	part2(&input);
	Ok(())
}

fn part1(input: &[u16])
{
	let mut goal: u16;
	for &num in input {
		goal = 2020 - num;
		if input.binary_search(&goal).is_ok() {
			let ans: u32 = num as u32 * goal as u32;
			println!("Part1: {}", ans);
			break;
		}
	}
}

fn part2(input: &[u16])
{
	for &a in input {
		for &b in input {
			for &c in input {
				if a + b + c == 2020 {
					println!("Part 2: {}", (a as u32) * (b as u32) * (c as u32));
					return;
				}
			}
		}
	}
}
