use std::error::Error;
use std::fs;

fn main() -> Result<(), Box<dyn Error>>
{
	let input = fs::read_to_string("../day5.txt")?;

	let mut ids = input.lines().map(|x| get_id(x)).collect::<Vec<u16>>();
	ids.sort_unstable();

	let part1 = ids.iter().max().unwrap();
	let part2 = ids.windows(2).find(|x| x[0] == (x[1] -2)).unwrap()[0] + 1;
	println!("Part 1: {}", part1);
	println!("Part 2: {}", part2);

	Ok(())
}

fn get_id(pass: &str) -> u16
{
	let mut score = 0;
	for ch in pass.chars() {
		score <<= 1;
		if ch == 'B' || ch == 'R'{ score |= 1 }
	}
	score
}
