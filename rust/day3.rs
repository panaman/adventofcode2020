use std::error::Error;
use std::fs;

const WIDTH: usize = 31;

fn main() -> Result<(), Box<dyn Error>>
{
	let input = fs::read_to_string("../day3.txt")?;
	let map: Vec<u8> = input.bytes().filter(|&x| x != 10).collect();

	let mut trees = 0usize;
	part1(&map, &mut trees);
	println!("Part 1: {}", trees);
	part2(&map, &mut trees);
	println!("Part 2: {}", trees);
	Ok(())
}

fn part1(map: &[u8], trees: &mut usize)
{
	let mut x = 0usize;
	let mut y = 0usize;

	while let Some(&num) = map.get(y * WIDTH + x) {
		if num == b'#' {
			*trees += 1;
		}
		y += 1;
		x = (x + 3) % WIDTH;
	}
}

fn part2(map: &[u8], trees: &mut usize)
{
	let mut curr = 0usize;
	let mut x = 0usize;
	let mut y = 0usize;

	// 5, 1
	while let Some(&num) = map.get(y * WIDTH + x) {
		if num == b'#' {
			curr += 1;
		}
		y += 1;
		x = (x + 5) % WIDTH;
	}
	*trees *= curr;

	// 7, 1
	curr = 0;
	x = 0;
	y = 0;
	while let Some(&num) = map.get(y * WIDTH + x) {
		if num == b'#' {
			curr += 1;
		}
		y += 1;
		x = (x + 7) % WIDTH;
	}
	*trees *= curr;

	// 1, 1
	curr = 0;
	x = 0;
	y = 0;
	while let Some(&num) = map.get(y * WIDTH + x) {
		if num == b'#' {
			curr += 1;
		}
		y += 1;
		x = (x + 1) % WIDTH;
	}
	*trees *= curr;

	// 1, 2
	curr = 0;
	x = 0;
	y = 0;
	while let Some(&num) = map.get(y * WIDTH + x) {
		if num == b'#' {
			curr += 1;
		}
		y += 2;
		x = (x + 1) % WIDTH;
	}
	*trees *= curr;
}
