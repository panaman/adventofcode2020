use std::collections::HashMap;
use std::error::Error;
use std::fs;

const COLORS: &[&str] = &["amb", "blu", "brn", "gry", "grn", "hzl", "oth"];

fn main() -> Result<(), Box<dyn Error>>
{
	let input = fs::read_to_string("../day4.txt")?;
	let book: Vec<&str> = input.split("\n\n").collect();

	let complete = part1(&book);
	println!("Part 1: {}", complete.len());
	part2(&complete);
	Ok(())
}

fn part1<'a>(book: &[&'a str]) -> Vec<&'a str>
{
	let mut valid_book: Vec<&str> = Vec::with_capacity(100);

	for pass in book {
		let is_valid = pass.contains("byr")
			&& pass.contains("iyr")
			&& pass.contains("eyr")
			&& pass.contains("hgt")
			&& pass.contains("hcl")
			&& pass.contains("ecl")
			&& pass.contains("pid");
		if is_valid { valid_book.push(pass); }
	}
	valid_book
}

fn part2(book: &[&str])
{
	// Flatten pass to [k,v,k,v,....]
	// iter over (k,v)
	// collect into hashmap
	let mut count = 0;
	for pass in book {
		let is_valid = pass
			.split_whitespace()
			.map(|x| x.split(':'))
			.flatten()
			.collect::<Vec<_>>()
			.chunks(2)
			.map(|x| (x[0], x[1]))
			.collect::<HashMap<_,_>>()
			.iter()
			.all(|(k, v)| validate(k, v));
		if is_valid { count += 1; }
	}
	println!("Part 2: {}", count);
}

fn validate(key: &str, val: &str) -> bool
{
	match key {
		"byr" => val.parse::<u16>().unwrap().wrapping_sub(1920) <= 82,
		"iyr" => val.parse::<u16>().unwrap().wrapping_sub(2010) <= 10,
		"eyr" => val.parse::<u16>().unwrap().wrapping_sub(2020) <= 10,
		"hgt" =>
			if let Some(x) = val.find("cm") {
				val[0..x].parse::<u16>().unwrap().wrapping_sub(150) <= 43
			}
			else if let Some(x) = val.find("in") {
				val[0..x].parse::<u16>().unwrap().wrapping_sub(59) <= 17
			}
			else { false },
		"hcl" => val.len() == 7,
		"ecl" => COLORS.iter().any(|&x| x == val),
		"pid" => val.len() == 9,
		_ => true
	}
}
