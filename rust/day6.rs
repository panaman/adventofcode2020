use std::collections::HashSet;
use std::error::Error;
use std::fs;

fn main() -> Result<(), Box<dyn Error>>
{
	let input = fs::read_to_string("../day6.txt")?;
	let forms = input.split("\n\n").collect::<Vec<_>>();

	let mut unique_set: Vec<HashSet<_>> = forms.iter()
		.map(|&form| form.chars().filter(|&x| x != '\n').collect::<HashSet<_>>())
		.collect();
/*
	unique_set.sort_by(|a, b| b.len().cmp(&a.len()));
	println!("{:?}\n{:?}", unique_set.get(0).unwrap(), unique_set.get(1).unwrap());
*/
	let part1: usize = unique_set.iter()
		.map(|x| x.len()).sum();

	//let part2: usize = unique_set.iter().;

	println!("Part 1: {}", part1);
	//println!("Part 2: {}", part2);

	Ok(())
}
